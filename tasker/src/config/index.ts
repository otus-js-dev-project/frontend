class Config {
  readonly backendUser = 'test'
  readonly backendPassword = 'password'
  readonly backendURL = 'http://localhost:8088/api/v1'
  readonly taskLimit = 4
}

export default new Config()
