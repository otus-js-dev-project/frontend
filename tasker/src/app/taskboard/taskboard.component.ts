import { Component, OnInit } from '@angular/core';
import { Task, TaskStatus } from '../../structures/task'
import { Board } from '../../structures/board'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import config from '../../config'

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.css']
})
export class TaskboardComponent implements OnInit {
  model: any = new Board([], new Task('', '', '', 0, null, TaskStatus.created))
  formCreateTaskOpened = false
  newTaskAccepted: boolean
  headers: any
  userId: number

  constructor(private http: HttpClient) {
    this.newTaskAccepted = false
    this.userId = 0
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(`${config.backendUser}:${config.backendPassword}`),
    })
  }

  valueEmittedFromChildComponent: string = '';

  parentEventHandlerFunction(valueEmitted: string){
    console.log('Delete task with id: ', valueEmitted);
    this.valueEmittedFromChildComponent = valueEmitted;

    this.getActiveTasks()
    this.newTaskAcceptedUpdate()
    this.formCreateTaskOpened = false
  }

  ngOnInit(): void {
    console.log('ngOnInit()');
    // TODO проверим авторизацию
    this.userId = parseInt(localStorage.getItem('userId') || '0')
    if (this.userId) {
      this.newTaskAcceptedUpdate()
      this.getActiveTasks()
    }
  }

  onSubmit(): void {
    console.log('onSubmit(): currentTask params: ', this.model.currentTask);
    const {title, description, goal} = this.model.currentTask

    const userId = localStorage.getItem('userId') || '0'
    this.http.post(`${config.backendURL}/create-task`, {
      userId: parseInt(userId),
      title,
      description,
      goal
    }, {headers: this.headers})
      .subscribe({
        next: this.createTaskHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  getActiveTasks() {
    // TODO по-хорошему тут нужен get-active-tasks
    this.http.post(`${config.backendURL}/get-history`, {
      userId: this.userId,
    }, {headers: this.headers})
      .subscribe({
        next: this.getTasksHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  onCreateTask() {
    console.log(`onCreateTask(): this.formCreateTaskOpened = ${this.formCreateTaskOpened} to ${!this.formCreateTaskOpened}`);
    this.formCreateTaskOpened = !this.formCreateTaskOpened
  }

  getTasksHandler(data: any) {
    console.log('getTasksHandler: data: ', data)
    if (data && data.tasks && data.tasks.length) {
      this.model.tasks = data.tasks.filter((task: Task) => task.state !== TaskStatus.finished && task.state !== TaskStatus.deleted).slice(0, 4)
      this.newTaskAcceptedUpdate()
    }
  }

  createTaskHandler(data: any) {
    console.log('createTaskHandler: data: ', data)
    this.getActiveTasks()

    this.formCreateTaskOpened = false
    this.model.currentTask = new Task('', '', '', 0, null, TaskStatus.created)
  }

  errorHandler(error: any) {
    console.error('Error:: ', error)
  }

  newTaskAcceptedUpdate() {
    this.newTaskAccepted = this.model.tasks.length < config.taskLimit
  }
}
