import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoryComponent } from "./history/history.component";
import { AuthComponent } from "./auth/auth.component";
import { TaskboardComponent } from "./taskboard/taskboard.component";

const routes: Routes = [
  { path: 'auth', component: AuthComponent },
  { path: 'taskboard', component: TaskboardComponent },
  { path: 'history', component: HistoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
