import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HistoryComponent } from './history/history.component';
import { TaskboardComponent } from './taskboard/taskboard.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TaskComponent } from './task/task.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HistoryComponent,
    TaskboardComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
