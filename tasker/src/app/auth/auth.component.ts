import { Component, OnInit } from '@angular/core';
import { User, Man } from '../../structures/interfases'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../config'

enum FormType {
  signIn = 0,
  signUp = 1
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements OnInit {
  isSignUpFailed: boolean
  formIn: User
  formUp: Man
  authorized: boolean
  submitted: boolean
  formType: FormType
  error: string
  headers: any

  constructor(private http: HttpClient) {
    this.isSignUpFailed = false
    this.authorized = false
    this.submitted = false
    this.formType = 0
    this.formIn = {
      username: '',
      password: ''
    }
    this.formUp = {
      name: '',
      lastname: '',
      password: '',
      username: ''
    }
    this.error = ''
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(`${config.backendUser}:${config.backendPassword}`),
    })
  }

  ngOnInit(): void {
    const authData = localStorage.getItem('userId')
    if (authData) {
      this.authorized = true
    }
  }

  signUp() {
    console.log('SignUp()')
    this.clearError()

    this.http.post(`${config.backendURL}/registration`, {
      name: this.formUp.name,
      lastname: this.formUp.lastname,
      password: this.formUp.password,
      username: this.formUp.username
    }, {headers: this.headers})
      .subscribe({
        next: this.registrationHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  async signIn(){
    console.log('SignIn()')
    this.clearError()

    this.http.post(`${config.backendURL}/authorization`, {
      password: this.formIn.password,
      username: this.formIn.username
    }, {headers: this.headers})
      .subscribe({
        next: this.authHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  logout(){
    console.log('Logout()');
    this.authorized = false
    this.submitted = false

    localStorage.removeItem('userId')
    this.clearError()
  }

  changeForm() {
    if (!this.formType) {
      this.formType = 1
      return
    }

    this.formType = 0
  }

  signed() {
    this.submitted = true
    this.authorized = true
  }

  authHandler(data: any) {
    console.log('authHandler: data: ', data);
    this.signed()
    localStorage.setItem('userId', data.userId.toString())
  }

  errorHandler(error: any) {
    console.log('authError: error: ', error);
    this.setError(error)
  }

  registrationHandler(data: any) {
    console.log('registrationHandler: data: ', data)
    this.signed()
    localStorage.setItem('userId', data.userId.toString())
  }

  setError(error: any) {
    this.error = JSON.stringify(error.error)
  }

  clearError() {
    this.error = ''
  }
}
