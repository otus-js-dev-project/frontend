import { Component, OnInit } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Task } from "../../structures/task"
import config from '../../config'

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  headers: any
  tasks: Task[]
  userId: number

  constructor(private http: HttpClient) {
    this.tasks = []
    this.userId = 0
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(`${config.backendUser}:${config.backendPassword}`),
    })
  }

  ngOnInit(): void {
    this.userId = parseInt(localStorage.getItem('userId') || '0')

    this.http.post(`${config.backendURL}/get-history`, {
      userId: this.userId,
    }, {headers: this.headers})
      .subscribe({
        next: this.getHistoryHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  getHistoryHandler(data: any) {
    if (data && data.tasks && data.tasks.length) {
      this.tasks = data.tasks
    }
  }

  errorHandler(error: any) {
    console.error('Error:: ', error)
  }
}
