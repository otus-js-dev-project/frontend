import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core'
import {HttpClient, HttpHeaders} from "@angular/common/http"
import {TaskStatus} from "../../structures/task"
import config from '../../config'

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() data: any
  @Input() index: number | undefined
  @Input() count: number | undefined

  @Output()
  buttonClicked: EventEmitter<string> = new EventEmitter<string>()

  timer: number
  tick: any
  isRun: boolean
  headers: any

  constructor(private http: HttpClient) {
    this.timer = 0
    this.tick = null
    this.isRun = false,
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(`${config.backendUser}:${config.backendPassword}`),
    })
  }

  ngOnInit(): void {
    console.log('TASK ngOnInit() ==> ', this.index);
    this.timer = this.data.timer
  }

  toPlay() {
    if (this.isRun) return

    console.log('toPlay()');
    this.isRun = true

    this.http.post(`${config.backendURL}/update-task`, {
      taskId: this.data.taskId,
      timer: this.timer || 1,
      state: TaskStatus.started
    }, {headers: this.headers})
      .subscribe({
        next: this.toPlayTaskHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  toPause() {
    console.log('toPause()');

    this.http.post(`${config.backendURL}/update-task`, {
      taskId: this.data.taskId,
      timer: this.timer,
      state: TaskStatus.paused
    }, {headers: this.headers})
      .subscribe({
        next: this.toPauseTaskHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  toStop() {
    console.log('Stop()');

    this.http.post(`${config.backendURL}/update-task`, {
      taskId: this.data.taskId,
      timer: this.timer,
      state: TaskStatus.deleted
    }, {headers: this.headers})
      .subscribe({
        next: this.toStopTaskHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  toComplete() {
    console.log('toComplete()');

    // TODO пока что просто пауза, нужно добавить визуальное отображение
    this.http.post(`${config.backendURL}/update-task`, {
      taskId: this.data.taskId,
      timer: this.timer || 1,
      state: TaskStatus.finished
    }, {headers: this.headers})
      .subscribe({
        next: this.toStopTaskHandler.bind(this),
        error: this.errorHandler.bind(this)
      })
  }

  toPlayTaskHandler() {
    this.tick = setInterval(() => {
      this.timer = this.timer + 1
      this.getTimerString()
    }, 1000)
  }

  toPauseTaskHandler() {
    clearInterval(this.tick)
    this.isRun = false
  }

  toStopTaskHandler() {
    this.buttonClicked.emit('Delete Some task now!')
  }

  errorHandler(error: any) {
    console.error('Error:: ', error)
  }

  getTimerString() {
    return `${this.getHours()}:${this.getMinutes()}:${this.getSeconds()}`
  }

  getHours(): string {
    const hours = Math.floor(this.timer / 3600)

    if (hours < 10) return `0${hours}`

    return `${hours}`
  }

  getMinutes(): string {
    const minutes = Math.floor((this.timer % 3600) / 60)

    if (minutes < 10) return `0${minutes}`

    return `${minutes}`
  }

  getSeconds(): string {
    const seconds = this.timer % 60

    if (seconds < 10) return `0${seconds}`

    return `${seconds}`
  }
}
