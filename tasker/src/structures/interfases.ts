export  interface User {
  username: string;
  password: string;
}

export  interface Man {
  name: string;
  lastname: string;
  password: string;
  username: string;
}
