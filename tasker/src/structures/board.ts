import { Task } from './task'

export class Board {
  constructor(
    public tasks: Task[],
    public currentTask: Task
  ) {}

}
