export enum TaskStatus {
  created = 'CREATED',
  started = 'STARTED',
  paused = 'PAUSED',
  finished = 'FINISHED',
  deleted = 'DELETED'
}

export class Task {
  constructor(
    public taskId: string,
    public title: string,
    public description: string,
    public timer: number,
    public goal: number | null,
    public state: TaskStatus
  ) {}
}
