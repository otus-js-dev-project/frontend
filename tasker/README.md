# frontend

Фронт для проекта `Tasker`

## Description

Сервис для развертывания фронт части для проекта `Tasker`

Возможности:
- Авторизация и регистрация
- Заведение задач (максимум 4)
- Управление задачами
- История задач

## Dependencies

- backend

## Getting started

- `npm run start`
